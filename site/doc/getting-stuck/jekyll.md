---
layout: default
title:  "Jekyll"
parent: Getting stuck
# grand_parent: 
has_children: false
categories: getting-stuck
permalink: /docs/getting-stuck/jekyll
nav_order: 2
---

<p align="center">
  <img src="/assets/img/jekyll.png" alt="jekyll.png" style="height:128px;width:250px;"/>
</p>

# Get stuck with Jekyll 

- [Init](#init-a-jekyll-site)
- [Run](#run-and-serve)

## Init a Jekyll site

  ```bash
  export SITENAME="codedoc" \
  && mkdir -p ${SITENAME}/site ${SITENAME}/vendor/bundle \
  && cat <<EOT >> ${SITENAME}/docker-compose.yaml
  version: '3'

  services:
    ${SITENAME}: 
      image: jekyll/jekyll
      container_name: ${SITENAME}
      environment:
          - JEKYLL_ENV=docker
          - JEKYLL_UID=${UID}
      command: jekyll serve --force_polling --livereload 
      ports:
          - 4000:4000
          - 35729:35729
      volumes:
        - ./site:/srv/jekyll
        - ./vendor/bundle:/usr/local/bundle
  EOT
  ```

Create a `Jekyll` site into a docker container, `SITENAME` SHOULD be change value to value that fit

Create the prerequisite dir tree & the docker compose file

## Run and serve

  ```bash
  cd ${SITENAME} \
  && docker-compose run --rm ${SITENAME} jekyll new . \
  && docker-compose up
  ```

Run the container once to init the `Jekyll` site and finally run the container and serve the site over the port `4000` with the live reload option