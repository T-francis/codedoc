---
layout: default
title:  "Server"
parent: Getting stuck
# grand_parent: 
has_children: false
categories: getting-stuck
permalink: /docs/getting-stuck/server
nav_order: 1
---

<p align="center">
  <img src="/assets/img/server.png" alt="server.png" style="height:128px;width:128px;"/>
</p>

# Get stuck with your own server 
- [Install the basics](#install-the-basics)
- [Create your user](#create-your-user)
- [Git config](#git-config)

## Install the basics

Install the very basics here

```bash
apt-get -y install wget curl sudo git --fix-missing
```

## Create your user

Create a user, make him `adm`, `sudo`, `root`

```bash
export username=john \
export pass=s3cr3t \
export email=john@doe.com \
&& useradd -m -s /bin/bash -p $pass $username \
&& usermod -a -G root,adm,sudo ${username} \
&& exit
```

## Git config

Basic user configuration for git 

```bash
git config --global user.name "${username}"
git config --global user.email "${email}"
```