---
layout: default
title:  "App init"
parent: Laravel
grand_parent: Getting stuck
has_children: false
categories: laravel
permalink: /docs/getting-stuck/laravel/app-init
nav_order: 1
---

<p align="center">
  <img src="/assets/img/laravel.png" alt="laravel.png" style="height:128px;width:128px;"/>
</p>

## Init a Laravel app

```
cd your/project \
    && composer create-project --prefer-dist laravel/laravel .
```

## Front end scaffold

https://medium.com/@taylorotwell/laravel-frontend-presets-eca312958def

```
php artisan preset none
```