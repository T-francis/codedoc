---
layout: default
title:  "Verifiy Mail"
parent: Laravel
grand_parent: Getting stuck
has_children: false
categories: laravel
permalink: /docs/getting-stuck/laravel/verify-mail
nav_order: 3
---

<p align="center">
  <img src="/assets/img/laravel.png" alt="laravel.png" style="height:128px;width:128px;"/>
</p>

## Verify Mail

https://medium.com/@pran.81/how-to-implement-laravels-must-verify-email-feature-in-the-api-registration-b531608ecb99

be sure user implements MustVerifyEmail
```php
class User extends Authenticatable implements MustVerifyEmail {}
```

## create VerificationController.php

```php
<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Foundation\Auth\VerifiesEmails;
use Illuminate\Http\Request;
use Illuminate\Auth\Events\Verified;

class VerificationController extends Controller {

    use VerifiesEmails;

    /**
    * Show the email verification notice.
    *
    */
    public function show(){
        //
    }

    /**
    * Mark the authenticated user's email address as verified.
    *
    * @param \Illuminate\Http\Request $request
    * @return \Illuminate\Http\Response
    */
    public function verify(Request $request) {

        $userID = $request['id'];
        $user = User::findOrFail($userID);
        $date = date(“Y-m-d g:i:s”);
        $user->email_verified_at = $date; // to enable the “email_verified_at field of that user be a current time stamp by mimicing the must verify email feature
        $user->save();
        return response()->json('Email verified!');

    }

    /**
    * Resend the email verification notification.
    *
    * @param \Illuminate\Http\Request $request
    * @return \Illuminate\Http\Response
    */
    public function resend(Request $request) {

    if ($request->user()->hasVerifiedEmail()) {
        return response()->json('User already have verified email!', 422);
        // return redirect($this->redirectPath());
    }

    $request->user()->sendEmailVerificationNotification();

    return response()->json('The notification has been resubmitted');
    // return back()->with('resent', true);

}

}
```

## create Notifications

create a folder `app/Notifications`
create a file named `VerifyEmail.php`

```php
<?php

namespace App\Notifications;

use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\URL;
use Illuminate\Auth\Notifications\VerifyEmail as VerifyEmailBase;

class VerifyEmail extends VerifyEmailBase {
    /**
    * Get the verification URL for the given notifiable.
    *
    * @param mixed $notifiable
    * @return string
    */
    protected function verificationUrl($notifiable) {
        return URL::temporarySignedRoute(
        'verificationapi.verify', Carbon::now()->addMinutes(60), ['id' => $notifiable->getKey()]
        ); // this will basically mimic the email endpoint with get request
    }

}
```

## User model

make `User` Model `use App\Notifications\VerifyEmail;` and add the following method
```php
    public function sendApiEmailVerificationNotification() {
        $this->notify(new VerifyApiEmail); // my notification
    }
```

## Verification route

specify the route `verificationapi.verify` in your api route 

```php
    Route::get('email/verify/{id}', 'VerificationController@verify')->name('verification.verify');

    Route::get('email/resend', 'VerificationController@resend')->name('verification.resend');
```

## Update AuthController

```php
<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Hash;
use Auth;
use Validator;
use Illuminate\Foundation\Auth\VerifiesEmails;
use Illuminate\Auth\Events\Verified;

class UsersApiController extends Controller {

    use VerifiesEmails;

    /**
    * login api
    * @return \Illuminate\Http\Response
    */
    public function login(){

        if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){

            $user = Auth::user();
            if($user->email_verified_at !== NULL){
                $success['message'] = 'Login successfull';
                return response()->json(['success' => $success], 200);
            } else {
                return response()->json(['error'=>'Please Verify Email'], 401);
            }

        } else {
            return response()->json(['error'=>'Unauthorised'], 401);
        }

    }

    /**
    * Register api
    * @return \Illuminate\Http\Response
    */
    public function register(Request $request){

        $validator = Validator::make($request->all(), [
        'name' => 'required',
        'email' => 'required|email',
        'password' => 'required',
        'c_password' => 'required|same:password',
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);
        }
        $input = $request->all();
        $input['password'] = Hash::make($input['password']);
        $user = User::create($input);
        $user->sendApiEmailVerificationNotification();
        $success['message'] = 'Please confirm yourself by clicking on verify user button sent to you on your email';
        return response()->json(['success'=>$success], 200);
    }

}
```

## Apply on a route

```php
Route::get('', function (Request $request) {
    return $request->user();
})->middleware('verified');
```