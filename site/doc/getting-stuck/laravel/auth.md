---
layout: default
title:  "Auth"
parent: Laravel
grand_parent: Getting stuck
has_children: false
categories: laravel
permalink: /docs/getting-stuck/laravel/auth
nav_order: 2
---

<p align="center">
  <img src="/assets/img/laravel.png" alt="laravel.png" style="height:128px;width:128px;"/>
</p>

## Auth api

https://w3path.com/create-rest-api-with-passport-authentication-laravel-6-tutorial/

```bash
composer require laravel/passport
```

Open config/app.php 
```php
  'providers' =>[
  Laravel\Passport\PassportServiceProvider::class,
  ],
```

Open app/providers/AppServiceProvider.php

make it use
```php
Use Schema; 
```

boot() method
```php
  public function boot() { 
    Schema::defaultStringLength(191); 
  }
```

Run migration
```bash
php artisan migrate
```

Make the install
```bash
php artisan passport:install
```

Open App/User.php file
```php
<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Contracts\Auth\MustVerifyEmail;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}

```

Open App/Providers/AuthServiceProvider.php

```php
<?php

namespace App\Providers;

use Laravel\Passport\Passport; 
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
         'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        Passport::routes(); 
    }
}

```

Open config/auth.php

```php
    'guards' => [
        'web' => [
            'driver' => 'session',
            'provider' => 'users',
        ],

        'api' => [
            'driver' => 'passport', 
            'provider' => 'users', 
        ],
    ],
```

Open api.php

```php
<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group( [ ], function(){
    Route::post('login', 'AuthController@login');
    Route::post('register', 'AuthController@register');
    Route::group(['middleware' => 'auth:api'], function() {
        Route::get('/user', function (Request $request) {
            return $request->user();
        });
    });
});
```
