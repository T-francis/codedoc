---
layout: default
title:  "Cors"
parent: Laravel
grand_parent: Getting stuck
has_children: false
categories: laravel
permalink: /docs/getting-stuck/laravel/cors
nav_order: 3
---

<p align="center">
  <img src="/assets/img/laravel.png" alt="laravel.png" style="height:128px;width:128px;"/>
</p>

## CORS

https://github.com/fruitcake/laravel-cors

```
composer require fruitcake/laravel-cors
```

Open app/Http/Kernel.php 

```php
protected $middleware = [
    // ...
    \Fruitcake\Cors\HandleCors::class,
];
```

publish the defaults congig (config/cors.php)
```bash
php artisan vendor:publish --tag="cors"
```


Open config/cors.php

```php
'paths' => ['api/*'],
```
