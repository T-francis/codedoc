---
layout: default
title: "MySQL Mariadb"
parent: Getting stuck
# grand_parent:
has_children: false
categories: getting-stuck
permalink: /docs/getting-stuck/mysql-mariadb
nav_order: 5
---

<p align="center">
  <img src="/assets/img/mysql.png" alt="mysql.png" width="128" height="128"/>
  <img src="/assets/img/mariadb.png" alt="mysql.png" width="128" height="128"/>
</p>

# Get stuck with MySQL Mariadb

- [Install](#install)
- [Basics](#basics)
  - [database](#database)
  - [user](#user)
  - [password](#password)
  - [Single base backup command](#single-base-backup-command)
  - [Multiple base backup script](#multiple-base-backup-script)
- [Utility](#utility)
- [Official Site](https://www.mysql.com/fr/)
- [Official Doc](https://dev.mysql.com/doc/)

## Install

```bash
# mysql server package
sudo apt install mysql-server

# # php ext package
# php-mysql

# # secure installation (optional)
# sudo mysql_secure_installation
```

## Basics

### Database

```sql
CREATE DATABASE IF NOT EXISTS dbname;
```

### User

```sql
-- local user
CREATE USER 'username'@'localhost' IDENTIFIED BY 'password';
GRANT ALL ON username.* TO 'username'@'localhost';
-- all database
-- GRANT ALL ON *.* TO 'username'@'localhost';

-- distant user
CREATE USER 'dbname'@'%' IDENTIFIED BY 'password';
GRANT ALL ON dbname.* TO 'username'@'%';
-- all database
-- GRANT ALL ON *.* TO 'username'@'%';
```

### Password

```sql
-- -- AS ROOT
-- local user
SET PASSWORD FOR 'username'@'localhost' = 'password';
-- distant user
SET PASSWORD FOR 'username'@'%' = 'password';

-- -- AS CURRENT USER
-- SET PASSWORD = 'password';
```

### Single base backup command

```bash
export username=john \
&& export password=waffle \
&& export basename=appdb \
&& export location=/dump \
&& mysqldump -u $username --password=$password "$basename" > $location/$basename.sql
```

### Multiple base backup script

```bash
#!/bin/bash
rootname=root
rootpassword=root
backupLocation=/path/to/dump

bases=`mysql -u $rootname --password=$rootpassword -e "show databases;" -B -s 2> /dev/null`

if [ -z "$bases" ];then echo "Access error OR no databases found"; exit 1; fi

echo "Backup start"
for base in $bases
do
    echo "Backing up $basename..."
    mysqldump -u $rootname --password=$rootpassword "$basename" > $backupLocation/$basename.sql
done

echo "Backup stop"
```

## Utility

### MySQLTuner

pearl script from [major](https://github.com/major/MySQLTuner-perl)

```bash
wget http://mysqltuner.pl/ -O mysqltuner.pl
wget https://raw.githubusercontent.com/major/MySQLTuner-perl/master/basic_passwords.txt -O basic_passwords.txt
perl mysqltuner.pl
```

### Mysql Mytop

```bash
sudo apt-get install mytop
sudo mytop -u username -p password # -d databasename for specific database
```

### Slow request log

```bash
sudo nano /etc/mysql/mysql.conf.d/mysqld.cnf

###
# CONFIG
log_slow_queries_log = 1
slow_query_log_file = /var/log/mysql/mysql-slow.log
long_query_time = 2
###
```
