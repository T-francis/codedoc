---
layout: default
title:  "Docker"
parent: Getting stuck
# grand_parent: 
has_children: false
categories: getting-stuck
permalink: /docs/getting-stuck/docker
nav_order: 1
---

<p align="center">
  <img src="/assets/img/docker.png" alt="docker.png" style="height:128px;width:128px;"/>
</p>

# Get stuck with docker 

- [Install](#install-docker-ce)
- [User perm](#give-docker-permission-to-a-user)
- [Install docker-compose](#install-docker-compose)
- [Shipping problem in no time](#shipping-problem-in-no-time)
  - [stop all container](#stop-all-container)
  - [delete all container](#delete-all-container)
  - [delete all image](#delete-all-image)
  - [create a network](#create-a-network)
- [Mess up with your Dockerfile like a boss](#mess-up-with-your-dockerfile-like-a-boss)
  - [create non root user](#create-non-root-user)
  - [add a process safety net](#add-a-process-safety-net)
- [Synchronize time](#synchronize-time)
  - [With environment variables](#with-environment-variables)
  
## Install docker CE

Use the convenience scripts from [docker git-hub](https://github.com/docker/docker-install)

```bash
curl -fsSL https://get.docker.com -o get-docker.sh
sh get-docker.sh
```

## Give docker permission to a user

While as root, give user permission to use docker, may throw warning on existing resource such as dir etc...  (unless being called john, SHOULD change the username)

```bash
export USERNAME="john";
usermod -aG docker $USERNAME;
mkdir /home/$USERNAME/.docker > /dev/null 2>&1;
chown -R $USERNAME:$USERNAME /home/$USERNAME/.docker;
chmod -R g+rwx /home/$USERNAME/.docker;
```
```

## Install docker-compose

While as root, install the latest docker-compose version

```bash
export COMPOSE_VERSION=`git ls-remote https://github.com/docker/compose | grep refs/tags | grep -oP "[0-9]+\.[0-9][0-9]+\.[0-9]+$" | tail -n 1` \
&& sh -c "curl -L https://github.com/docker/compose/releases/download/${COMPOSE_VERSION}/docker-compose-`uname -s`-`uname -m` > /usr/local/bin/docker-compose" \
&& chmod +x /usr/local/bin/docker-compose \
&& sh -c "curl -L https://raw.githubusercontent.com/docker/compose/${COMPOSE_VERSION}/contrib/completion/bash/docker-compose > /etc/bash_completion.d/docker-compose" \
&& docker-compose --version
```

## Shipping problem in no time

### stop all container
```bash 
sudo docker stop $(sudo docker ps -a -q)
```

### delete all container
```bash 
sudo docker rm $(sudo docker ps -a -q)
```

### delete all image
```bash 
sudo docker rmi $(sudo docker images -q)
```

### create a network
```bash 
docker network create ${name}
```

## Mess up with your Dockerfile like a boss

### create non root user

```dockerfile 
RUN groupadd -r nodejs \
   && useradd -m -r -g nodejs nodejs
USER nodejs
```

### add a process safety net

```dockerfile 
ADD https://github.com/Yelp/dumb-init/releases/download/v1.1.1/dumb-init_1.1.1_amd64 /usr/local/bin/dumb-init
RUN chmod +x /usr/local/bin/dumb-init

CMD ["dumb-init", "node", "index.js"]
```

## Synchronize time

### With environment variables

```bash 
#docker run
docker run -e TZ=America/New_York
#docker-compose
  environment:
    - TZ=America/New_York
```

### With dockerfile

```dockerfile 
RUN echo "Europe/Stockholm" > /etc/timezone
RUN dpkg-reconfigure -f noninteractive tzdata
```

### With volumes

```dockerfile
#docker-compose
  volumes:
    - "/etc/timezone:/etc/timezone:ro"
    - "/etc/localtime:/etc/localtime:ro"
```