---
layout: default
title:  "Snippets"
# parent: 
# grand_parent: 
has_children: true
categories: snippets
permalink: /docs/snippets
nav_order: 2
---

<p align="center">
  <img src="/assets/img/code-purple-gradient.png" alt="code-purple-gradient.png" style="height:128px;width:128px;"/>
</p>

# Snippets

The place where all those commands that I love copy pasting day after day are resting in peace
{: .fs-5 .fw-300 }