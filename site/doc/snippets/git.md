---
layout: default
title:  "Git"
parent: Snippets
# grand_parent: 
has_children: false
categories: snippets
permalink: /docs/snippets/git
nav_order: 2
---

<p align="center">
  <img src="/assets/img/git.png" alt="git.png" style="height:128px;width:128px;"/>
</p>

# Screw it with git

- [Every-one-know](#every-one-know)
  - [Official Site ](https://git-scm.com/)
  - [Official Doc](https://git-scm.com/doc)
- [Gitignore templates](#gitignore-templates)
  - [vscode](#vscode)
  - [jetbrains](#jetbrains)
  - [eclipse](#eclipse)

## Every one know

```bash
# git diff
git diff
git diff a45Fv54
git diff a45Fv54..gf4244g

# config
git help config
git config --list
git config --global user.name "john"
git config --global user.email "john@doe.com"
git config --global color.ui true

git checkout a45Fv54 # get back to that commit

# the underestimate git log
git help log
git log
git log --oneline
git log --oneline readme.md
git log -p readme.md # q to quit
```

## Gitignore templates

### vscode

```text
## [ VSCODE ]
.vscode/*
!.vscode/settings.json
!.vscode/tasks.json
!.vscode/launch.json
!.vscode/extensions.json
##/
```

### jetbrains

```text
## [ JetBrains ]
# Covers JetBrains IDEs: IntelliJ, RubyMine, PhpStorm, AppCode, PyCharm, CLion, Android Studio and Webstorm
# Reference: https://intellij-support.jetbrains.com/hc/en-us/articles/206544839

# idea folder CUSTOM RULES
.idea/

# User-specific stuff:
.idea/**/workspace.xml
.idea/**/tasks.xml
.idea/dictionaries

# Sensitive or high-churn files:
.idea/**/dataSources/
.idea/**/dataSources.ids
.idea/**/dataSources.xml
.idea/**/dataSources.local.xml
.idea/**/sqlDataSources.xml
.idea/**/dynamic.xml
.idea/**/uiDesigner.xml

# Gradle:
.idea/**/gradle.xml
.idea/**/libraries

# CMake
cmake-build-debug/
cmake-build-release/

# Mongo Explorer plugin:
.idea/**/mongoSettings.xml

## File-based project format:
*.iws

## Plugin-specific files:

# IntelliJ
out/

# mpeltonen/sbt-idea plugin
.idea_modules/

# JIRA plugin
atlassian-ide-plugin.xml

# Cursive Clojure plugin
.idea/replstate.xml

# Crashlytics plugin (for Android Studio and IntelliJ)
com_crashlytics_export_strings.xml
crashlytics.properties
crashlytics-build.properties
fabric.properties
##/
```

### eclipse
```text
## [ECLIPSE]
# source : https://github.com/github/gitignore/blob/master/Global/Eclipse.gitignore

.project
.metadata
bin/
tmp/
*.tmp
*.bak
*.swp
*~.nib
local.properties
.settings/
.loadpath
.recommenders

# External tool builders
.externalToolBuilders/

# Locally stored "Eclipse launch configurations"
*.launch

# PyDev specific (Python IDE for Eclipse)
*.pydevproject

# CDT-specific (C/C++ Development Tooling)
.cproject

# Java annotation processor (APT)
.factorypath

# PDT-specific (PHP Development Tools)
.buildpath

# sbteclipse plugin
.target

# Tern plugin
.tern-project

# TeXlipse plugin
.texlipse

# STS (Spring Tool Suite)
.springBeans

# Code Recommenders
.recommenders/

# Scala IDE specific (Scala & Java development for Eclipse)
.cache-main
.scala_dependencies
.worksheet
##/
```
