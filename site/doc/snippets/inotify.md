---
layout: default
title:  "Inotify"
parent: Snippets
# grand_parent: 
has_children: false
categories: snippets
permalink: /docs/snippets/inotify
nav_order: 2
---

<p align="center">
  <img src="/assets/img/inotify.png" alt="inotify.png" style="height:128px;width:128px;"/>
</p>

# Kill it with inotify

- [Install](#install)

## Install

```bash
 sudo apt-get install inotify-tools
```

## Example

### Naïve global watcher example

```bash
#!/bin/bash

# -r recursive -m monitoring -e event to capture –exclude log file –format « %e|%w%f »
# output event type and filename separated by |
# CREATE|/home/user/tmp/fileName
# MODIFY|/home/user/tmp/fileName
# DELETE|/home/user/tmp/fileName

inotifywait -m -r -e create -e delete -e moved_to -e modify --exclude "*.log" \
            --format "%e|%w%f" /home/user/rep \
            | while read res
do
  event=`echo $res | sed s/\|.*$//`
  src=`echo $res | sed s/^.*\|//`
  case "$event" in
    CREATE)
      echo "file create event"
      ;;
    CREATE,ISDIR)
      echo "directory create event"
      ;;
    DELETE)
      echo "delete event"
      ;;
    DELETE,ISDIR)
      echo "delete a dir event"
      ;;
    MOVED_TO)
      echo "move event"
      ;;
    MODIFY)
      echo "modification event"
      ;;
  esac
done
```

### Basic file logger
```bash
#!/bin/bash

location=`pwd`
logfile=/home/$USER/file.activity.log

# -mrq recursive monitoring  --exclude ".ext"
inotifywait -mrq -e create -e delete -e moved_to -e modify \
            --format "%e %w%f" $location \
            | while read filename
do
  # logfile creation
  [[ ! -e ${logfile} ]] && touch ${logfile}
  # write any modification into log
  echo "[`date +%Y-%m-%d`][`date +%T`] => $filename" >> ${logfile}
done
```