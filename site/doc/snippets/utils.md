---
layout: default
title:  "Utils"
parent: Snippets
# grand_parent: 
has_children: false
categories: snippets
permalink: /docs/snippets/utils
nav_order: 3
---

<p align="center">
  <img src="/assets/img/cog-yellow.png" alt="cog-yellow.png" style="height:128px;width:128px;"/>
</p>

# Utils

All the function I secretly use

```bash
#!/bin/bash

function scan-range() {
    for ((i=8000;i<=8050;i++)); do [[ $(is-listening $i) = true ]] && availablePort+=($i); done;
    echo ${availablePort[@]};
}

function generate-password() {
    [[ -z ${1} ]] && local length=13 || local length=${1}
    echo `head /dev/urandom | tr -dc A-Za-z0-9 | head -c ${length}`;
}

function convert-all-to-lf() {
    find . -name '*.*sh' -print0 2>/dev/null|
    while IFS= read -r -d $'\0' file; do sed -i 's/\r$//' ${file} && [[ -f "${file}.bak" ]] && rm "${file}.bak"; done;
    exit 0;
}

function in-array() {
    local n=$#;
    local value=${!n};
    for ((i=1;i < $#;i++)); do [[ "${!i}" == "${value}" ]] echo true && return 0; done;
    echo false && return 1;
}

function eof-is-newline() {
    [[ -z "$(tail -c 1 "${1}")" ]] && echo true || echo false;
}

function is-listening() {
    [[ -z "$(netstat -tulpn 2>/dev/null | grep ${1})" ]] && echo true || echo false;
}

function is-installed() {
    dpkg -s ${1} &> /dev/null; [[ $? -eq 0 ]] && echo true || echo false;
}

function network-exist() {
    [[ ! "$(docker network ls | grep ${1})" ]] && echo false || echo true;
}

function image-exist() {
    [[ "$(docker images -q $image 2> /dev/null)" == "" ]] && echo false || echo true;
}

function docker-connect() {
    docker exec -it "$@" sh -c "stty rows 50 && stty cols 150 && bash";
}

function docker-list() { {% comment %} cmd is escaped for liquid {% endcomment %}
    docker ps --format {{ "'{{ .Names " }}}}' | while read line; do echo "$line" done;
}
```