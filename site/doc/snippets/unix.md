---
layout: default
title:  "Unix"
parent: Snippets
# grand_parent: 
has_children: false
categories: unix
permalink: /docs/snippets/unix
nav_order: 1
---

<p align="center">
  <img src="/assets/img/terminal.png" alt="terminal.png" style="height:128px;width:128px;"/>
</p>

# Unix

- [Basics](#basics)
  - [basic option handle](#basic-option-handle)
  - [is it ?](#is-it)
  - [create an admin user](#create-an-admin-user)
  - [dayli cmd](#dayli-cmd)
- [File-and-string-manipulation](#file-and-string-manipulation)
  - [Substring](#substring)
  - [CLRF to LF](#clrf-to-lf)
  - [Play with .csv](#play-with-csv)
- [NTP sync](#ntp-sync)
- [SSH](#ssh)
- [Samba](#samba)
- [Ttfb checker](#ttfb-checker)

## Basics

- [basic-option-handle](#basic-option-handle)
- [create-an-admin-user](#create-an-admin-user)

### basic option handle

```bash
#!/bin/bash

function help(){
echo -e "Script usage\n"
echo -e "--help or -h: call help function"
echo -e "--sayHello : call sayHello function"
}

function sayHello(){
	echo "hello $1"
	exit 0
}

# if no args, displaying help
if [ $# -eq 0 ];then help; fi

OPTS=$( getopt -o h,s: -l help,sayHello: -- "$@" )
if [ $? != 0 ];then	exit 1; fi
eval set -- "$OPTS"

while true ;
do
	case "$1" in
		--help | -h)
			help
			exit 0
			;;
		--sayHello | -s)
			sayHello $2
			# more code here
			shift 2
			;;
		--)
			shift;
			break
			;;
	esac
done
exit 0
```

### is it

```bash
# string
[ $str1 = $str2 ] # if equal
[ $str1 != $str2 ] # if different
[ -z $str ] # if empty
[ -n $str ] # if not empty
# numbers
[ $int1 -eq $int2 ] # if equal
[ $int1 -ne $int2 ] # if different
[ $int1 -le $int2 ] # if equal or lower <=
[ $int1 -lt $int2 ] # if lower <
[ $int1 -ge $int2 ] # if equal or greater >=
[ $int1 -gt $int2 ] # if greater >
# file/dir
[ -e $fileName ] # if exist
[ -d $fileName ] # if is a dir 
[ -f $fileName ] # if is a file
[ -L $fileName ] # if is a symlink
[ -r $fileName ] # if is readable
[ -w $fileName ] # if is writtable
[ -x $fileName ] # if is executable
[ $file1 -nt $file2 ] # if is newer
[ $file1 -ot $file2 ] # if is older
```

### create an admin user

```bash
# here we are again my old friend john
export username=john \
&& adduser $username \
&& usermod -a -G root,adm,sudo $username \
&& exit
```

### dayli cmd

```bash
df -h # see disk space
sudo locate *.log* # locate almost every logfile on host
sudo compgen -g # list all group
sudo compgen -u # list all user
sudo getent group groupName # list all user of a group
```

### alias

```bash
alias gh='history | grep '
```

## File and string manipulation

- [substring](#substring)
- [clrf to lf](#clr-to-lf)

### substring

stolen from:[stackoverflow](https://stackoverflow.com/a/16623897)

```ini
string="hello-shiny-world"
prefix="hello-"
suffix="-world"
rest=${string#"$prefix"};rest=${rest%"$suffix"}
echo "$rest" # "shiny"
```

### CLRF to LF

```bash
sed -i -e 's/\r$//' $filename
```

### Play with .csv

removing the comment
```bash
grep -v "^#" input.csv > input-nocomments.csv
```

removing the header
```bash
cat input.csv | sed "1 d" > noheader.csv
```

both in one line
```bash
cat input.csv | grep -v "^#" | sed "1 d" > onlydata.csv
```

count row (header and comment)
```bash
wc -l input.csv
```

count row (without header and comment)
```bash
cat input.csv | grep -v "^#" | sed "1 d" | wc -l
```

count column (without comment)
```bash
cat input.csv | grep -v "^#" | awk "{print NF}" FS=,
```

count column as unique (without comment)
```bash
cat input.csv | grep -v "^#" | awk "{print NF}" FS=, | uniq
```

merge multiple csv (same column)
```bash
cat input1.csv > combined.csv
cat input2.csv | sed "1 d" >> combined.csv
cat input3.csv | sed "1 d" >> combined.csv
```

extract row based on value
```bash
# will strictly grep only "pizza"
grep "pizza" input.csv > pizza.csv 
# for insensitive case, in example, will also ctach "pizza."
grep -i "pizza" input.csv >> pizza.csv
```

extract row based on value with header
```bash
head -n 1 input.csv > pizza.csv
grep "pizza" input.csv >> pizza.csv
```

extract colomn (2.4.6 in example)
```bash
cut -d , -f 2,4-6 input.csv
```

## NTP sync
```
sudo apt-get update \
	&& sudo apt-get install ntp ntpdate \
	&& sudo /etc/init.d/ntp stop \
	&& sudo ntpdate fr.pool.ntp.org \
	&& sudo /etc/init.d/ntp start
```

## SSH

```bash
# create a group and dedicated it for ssh
sudo addgroup sshusers
sudo adduser userName sshusers

# edit config
sudo vi /etc/ssh/sshd_config
########
# What ports, IPs and protocols we listen for /!\ Be carefull about what port you choose to listen to
Port 22
# Authentication:
LoginGraceTime 30
PermitRootLogin no
StrictModes yes
MaxStartups 10:30:60
AllowGroups sshusers
########

sudo service ssh restart
```

## Samba

```bash
sudo apt install samba # install
sudo smbstatus # status
sudo smbpasswd -a username # adduser
sudo smbpasswd -x username # delete user
sudo systemctl restart smbd # restart

# config
sudo nano /etc/samba/smb.conf #Config
########
[global]
## Browsing/Identification ###
# Change this to the workgroup/NT-domain name your Samba server will part of
	workgroup = WORKGROUP
	netbios name = sambaHostMachineName
	public = yes
	security = user

[folderNameOnNetwork]
path = /pathTo/folderToShare/
read only = no
writeable = yes
valid users = sambaUserName
comment = description of the share
########

# mount remote shared a dir
sudo mount //192.168.1.1/pathTo/sharedFolder /pathTo/sharedFolder/mountFolder/ -o username=userName
```

## ttfb checker

```bash
#!/bin/bash
today=$(date +'%Y-%m-%d')
function run(){
  IFS=$'\n'
  for url in $(cat < "./urls"); do
    echo "Go for : $url"
    ttfb=$( curl -o /dev/null -H 'Cache-Control: no-cache' -s -w "%{time_starttransfer}\n" $url )
    if ! [  -f ./logs/$today-hits.log ]; then
      touch ./logs/$today-hits.log 
    fi
    echo "$url=$ttfb" >> ./logs/$today-hits.log
  done
}

run
```