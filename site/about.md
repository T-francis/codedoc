---
layout: default
title: About
permalink: /about
nav_order: 3
---

This is a simple site built with [jekyll](jekyll-organization) and it use the [just the doc](https://pmarsceill.github.io/just-the-docs/) theme. You can find out more info as basic Jekyll usage documentation at [jekyllrb.com](https://jekyllrb.com/)
