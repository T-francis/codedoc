# codedoc

## Run

### development 

- set the UID into .env
```bash
mv .env.dist .env # and manually set your UID
```

- start

```bash
docker-compose up # -d for detached mode
```

- (re) generate the search index
```bash
docker exec -ti --user jekyll codedoc /bin/bash -c "cd /srv/jekyll/ && bundle exec just-the-docs rake search:init"
```